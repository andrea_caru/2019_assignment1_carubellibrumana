# STUDENTS
+ 808374 Brumana Mattia
+ 803192 Carubelli Andrea


Link to the repository on GitLab:
+ [https://gitlab.com/andrea_caru/2019_assignment1_carubellibrumana/](https://gitlab.com/andrea_caru/2019_assignment1_carubellibrumana/)

# APPLICATION
An application that allows users to rent cars for a limited period of time.

The database consists of three tables: 
+ Car: a list of cars that can be rented;
+ Rental: a list of all the rentals with their starting date and ending date.
+ Client: a list of users that can rent a car.

The database used is a MySQL database, and it interfaces with the Python application via the "SQLite" library.

The user can rent a car by entering its Fiscal Code and the car license plate.

# DEVOPS
## Containerization/Virtualization
Use of a container with a Docker image for the application (docker image python:3.7).

## Continuous Integration and Continuous Deployment CI/CD
The GitLab pipeline for the CI/CD is divided into 5 stages:
+ *build*: we combine the source code and its dependencies to build a runnable instance of our application. This stage of the CI/CD pipeline builds the Docker containers;
+ *verify*: stage that validates the code. We use Pyflakes, a simple program that analyzes programs and detects various errors by parsing the source file;
+ *unit-test*: we run automated tests to validate the correctness of our code and the behaviour of the application;
+ *release*: Docker images are renamed with the tag: *latest* and uploaded to the registry;
+ *deploy*: if all stages are successfully executed, the release image is used to update containers running on [https://rent-a-car-brumana-carubelli.herokuapp.com](https://rent-a-car-brumana-carubelli.herokuapp.com)

### Build Stage
+ *docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com*
+ *docker build -t $CONTAINER_IMAGE .*
+ *docker push $CONTAINER_IMAGE*

### Verify Stage
+ *pyflakes app.py*
+ *pyflakes database_functions.py*
+ *pyflakes test.py*

### Unit-Test Stage
+ *pytest test.py*

### Release Stage
+ *docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com*
+ *docker pull $CONTAINER_IMAGE*
+ *docker tag $CONTAINER_IMAGE $CONTAINER_RELEASE_IMAGE*
+ *docker push $CONTAINER_RELEASE_IMAGE*

### Deploy Stage
+ *dpl --provider=heroku --app=$HEROKU_APP_STAGING --api-key=$HEROKU_API_KEY*

Each stage is executed at each commit through the *.gitlab-ci.yml* configuration file.