from flask import Flask, render_template, request, flash, redirect, url_for
import os
import database_functions
import sqlite3

database_functions.initialize_database()
app = Flask(__name__)
app.secret_key = 'random string'

@app.route("/")
def index():
    database = sqlite3.connect('database/rent_a_car.db')
    car = database_functions.show_car()
    database.close()

    return render_template(
        "index.html", main=True, car = car
    )

@app.route("/addRental", methods=['GET', 'POST'])
def addRental():
    #Get datas from html form.
    fiscal_code = request.form['f_code']
    car_plate = request.form['car_plate']
    end_date = request.form['end_date']

    #Check if insterted datas are correct.
    cfc = database_functions.check_fiscal_code(fiscal_code)
    cc = database_functions.check_client(fiscal_code)
    cp = database_functions.check_plate(car_plate)
    cpe = database_functions.check_plate_existing(car_plate)
    ced = database_functions.check_end_date(end_date)

    if cfc and cc and cp and cpe and ced:
        database_functions.add_rent(fiscal_code, car_plate, end_date)
    else:
        flash("ERROR: Something went wrong during the rental. Please check your inserted datas and if you are signed in.")

        return redirect(url_for('index'))

    car = database_functions.show_car()         
    return render_template(
        "index.html", main=True, car = car
    )

@app.route("/showRental", methods=['GET', 'POST'])
def showRental():
    #Get datas from html form.
    fiscal_code_2 = request.form['f_code_2']

    #Check if insterted datas are correct.
    cfc = database_functions.check_fiscal_code(fiscal_code_2)
    cc = database_functions.check_client(fiscal_code_2)
   
    if cfc and cc:
        rental = database_functions.show_rental(fiscal_code_2)
    else:
        flash("ERROR: Your Fiscal Code isn't in the database. Please register below.")
            
        return redirect(url_for('index'))

    car = database_functions.show_car()
    return render_template(
        "index.html", main=True, car = car, rental = rental
    )

@app.route("/signIn", methods=['GET', 'POST'])
def signIn():
    #Get datas from html form.
    fiscal_code = request.form['f_code']
    name = request.form['name']
    surname = request.form['surname']
    date_birth = request.form['date_birth']
    place_birth = request.form['place_birth']
    sex = request.form['sex']

    #Check if insterted datas are correct.
    cfc = database_functions.check_fiscal_code(fiscal_code)
    cn = database_functions.check_name(name)
    cs = database_functions.check_surname(surname)
    cd = database_functions.check_date(date_birth)
    cp = database_functions.check_place_of_birth(place_birth)
    cse = database_functions.check_sex(sex)

    if cfc and cn and cs and cd and cp and cse:
        database_functions.add_client(fiscal_code, name, surname, date_birth, place_birth, sex)
    else:
        flash("ERROR: An error occurred during the registration. Please check your inserted data.")
            
        return redirect(url_for('index'))

    car = database_functions.show_car()
    return render_template(
        "index.html", main=True, car = car
    )

@app.route("/addCar", methods=['GET', 'POST'])
def addCar():
    #Get datas from html form.
    car_plate = request.form['car_plate']
    brand = request.form['brand']
    model = request.form['model']
    price = request.form['price']

    #Check if insterted datas are correct.
    cp = database_functions.check_plate(car_plate)
    cpe = database_functions.check_plate_existing(car_plate)
    print(str(car_plate) + str(cpe))
    if cp:
        if cpe==False:
            flash("ERROR: An error occurred during the registration of the car. The car already exists. Please change car plate.")
            return redirect(url_for('index'))

        database_functions.add_car(car_plate, brand, model, price)
    else:
        flash("ERROR: An error occurred during the registration of the car. Please check the inserted plate.")
            
        return redirect(url_for('index'))
    
    car = database_functions.show_car()
    return render_template(
        "index.html", main=True, car = car
    )    


if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(host='0.0.0.0', port=port)