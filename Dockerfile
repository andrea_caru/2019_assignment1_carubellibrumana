# Use an official Python runtime as a parent image
FROM python:3.7

# Copy the current directory contents into the container at /application
COPY . /application

# Set the working directory to /application
WORKDIR /application

RUN apt-get update
#RUN apt-get install all needed packages
RUN pip install -r requirements.txt
RUN pip install mysql

# Run app.py when the container launches
#CMD ["python", "./application/test.py"]

CMD python /application/app.py