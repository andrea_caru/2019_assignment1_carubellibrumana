import re
from datetime import date
import sqlite3
import codicefiscale as cf

def initialize_database():
    #Open database connection.
    connection = sqlite3.connect('database/rent_a_car.db')
    #Prepare a cursor to work with database.
    cursor = connection.cursor()

    #If tables don't exist in the database, we create new tables.
    #Otherwise, we use the existing tables.
    cursor.execute("CREATE TABLE IF NOT EXISTS car(plate VARCHAR(7) primary key, \
        brand VARCHAR(20), model VARCHAR(20), price INT(11));")
    cursor.execute("CREATE TABLE IF NOT EXISTS client(fiscal_code VARCHAR(16) primary key, \
        name VARCHAR(20), surname VARCHAR(20), date_of_birth DATE, \
        place_of_birth VARCHAR(30), sex CHAR(1));")
    cursor.execute("CREATE TABLE IF NOT EXISTS rental(plate VARCHAR(7), fiscal_code VARCHAR(16), \
        start_date DATE, end_date DATE, FOREIGN KEY(plate) REFERENCES car(plate), \
        FOREIGN KEY(fiscal_code) REFERENCES client(fiscal_code));")

    #We generate some records and put them in tables.
    #cursor.execute("INSERT INTO car(plate, brand, model, price) \
    #VALUES('FC455DZ', 'Alfa Romeo', 'Giulietta', 4999)")
    #connection.commit()
    #cursor.execute("INSERT INTO car(plate, brand, model, price) \
    #VALUES('EY378YQ', 'Mercedes-Benz', 'A220', 6999)")
    #connection.commit()
    #cursor.execute("INSERT INTO client(fiscal_code, name, surname, \
    #date_of_birth, place_of_birth, sex) VALUES('RSSMRA84L01A794Q', \
    #'Mario', 'Rossi', '1984-07-01', 'Bergamo', 'M')")
    #connection.commit()
    #cursor.execute("INSERT INTO client(fiscal_code, name, surname, \
    #date_of_birth, place_of_birth, sex) VALUES('VRDMRA92L54A271D', \
    #'Maria', 'Bianchi', '1978-09-14', 'Ancona', 'F')")
    #connection.commit()
    #cursor.execute("INSERT INTO rental(plate, fiscal_code, start_date,\
    #end_date) VALUES('FC455DZ', 'VRDMRA92L54A271D', '2019-08-21', '2019-12-31')")
    #date_of_birth, place_of_birth, sex) VALUES('RSSMTT95F07A794G', \
    #'Matteo', 'Rossi', '1995-06-07', 'Bergamo', 'M')")
    #connection.commit()
    #cursor.execute("INSERT INTO client(fiscal_code, name, surname, \
    #date_of_birth, place_of_birth, sex) VALUES('BNCMRA78P54A271O', \
    #'Maria', 'Bianchi', '1978-09-14', 'Ancona', 'F')")
    #connection.commit()
    #cursor.execute("INSERT INTO rental(plate, fiscal_code, start_date,\
    #end_date) VALUES('FC455DZ', 'RSSMTT95F07A794G', '2019-08-21', '2019-12-31')")
    #connection.commit()


def show_car():
    """Show car table"""
    #Open database connection.
    connection = sqlite3.connect('database/rent_a_car.db')

    #Prepare a cursor to work with database.
    cursor = connection.cursor()

    cursor.execute("SELECT * from car")
    return cursor.fetchall()


def show_client():
    """Get connection from the server"""
    #Open database connection.
    connection = sqlite3.connect('database/rent_a_car.db')
    
    #Prepare a cursor to work with database.
    cursor = connection.cursor()

    cursor.execute("SELECT * from client")
    return cursor.fetchall()


def show_rental(fiscal_code):
    """Get connection from the server"""
    #Open database connection.
    connection = sqlite3.connect('database/rent_a_car.db')

    #Prepare a cursor to work with database.
    cursor = connection.cursor()

    cursor.execute("SELECT * from rental WHERE fiscal_code='" + fiscal_code + "'")
    return cursor.fetchall()


def check_client(user):
    """Check if a client is already insert into database"""
    #Open database connection.
    connection = sqlite3.connect('database/rent_a_car.db')

    #Prepare a cursor to work with database.
    cursor = connection.cursor()

    cursor.execute("SELECT fiscal_code FROM client")
    clients = cursor.fetchall()
    correct_client = False

    #Check if the client Fiscal Code is in the database.
    for row in clients:
        if row[0] == user:
            correct_client = True

    return correct_client


def add_client(fiscalcode, name, surname, datebirth, placebirth, sex):
    """Get connection from the server"""
    #Open database connection.
    connection = sqlite3.connect('database/rent_a_car.db')

    #Prepare a cursor to work with database.
    cursor = connection.cursor()

    #Insert new client.
    try:
        cursor.execute("INSERT INTO client(fiscal_code, name, surname, \
            date_of_birth, place_of_birth, sex) \
            VALUES('"+ fiscalcode + "','" + name + "','" + surname + "','" \
            + datebirth + "','" + placebirth + "','" + sex + "');")
        connection.commit()
        print("New user has been created!")
    except:
        print("Sorry! An error occurred during the Registration!")

def add_car(plate, brand, model, price):
    """Get connection from the server"""
    #Open database connection.
    connection = sqlite3.connect('database/rent_a_car.db')

    #Prepare a cursor to work with database.
    cursor = connection.cursor()

    #Insert new client.
    try:
        cursor.execute("INSERT INTO car(plate, brand, model, \
            price) \
            VALUES('"+ plate + "','" + brand + "','" + model + "'," \
            + price + ");")
        connection.commit()
        print("New car has been inserted!")
    except:
        print("Sorry! An error occurred during the Car Registration!")

#check if fiscal code is valid
def check_fiscal_code(code):
    """Check if a fiscal code is valid"""
    check = cf.isvalid(code)
    return check


#Check if Name is valid.
def check_name(nome):
    """Check if a name is valid"""
    if not nome:
        return False

    for i in range(0, len(nome)):
        if (not(nome[i] >= 'a' and nome[i] <= 'z') \
            and not (nome[i] >= 'A' and nome[i] <= 'Z') \
            and not nome[i] == ' '):
            return False

    return True


#Check if Surname is valid.
def check_surname(cognome):
    """Check if a surname is valid"""
    if not cognome:
        return False

    for i in range(0, len(cognome)):
        if (not (cognome[i] >= 'a' and cognome[i] <= 'z') \
            and not (cognome[i] >= 'A' and cognome[i] <= 'Z') \
            and not cognome[i] == ' '):
            return False

    return True


#Check if Place of Birth is valid.
def check_place_of_birth(place):
    """Check if a place of birth is valid"""
    if not place:
        return False

    for i in range(0, len(place)):
        if (not (place[i] >= 'a' and place[i] <= 'z') \
            and not (place[i] >= 'A' and place[i] <= 'Z') \
            and not place[i] == ' '):
            return False

    return True


#Check if Date is valid.
def check_date(DATE):
    """Check if a date is valid"""
    if not DATE:
        return False

    pattern = re.compile("([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))")
    if not bool(pattern.match(DATE)):
        return False

    return True


#Check if Sex is valid.
def check_sex(sex):
    """Check if sex is valid"""
    if not sex:
        return False
    if len(sex) > 1:
        return False
    if (not (sex[0] >= 'a' and sex[0] <= 'z') \
        and not (sex[0] >= 'A' and sex[0] <= 'Z')):
        return False
    return True


#Check if Plate is valid.
def check_plate(plate):
    """Check if plate is valid"""
    if not plate:
        return False

    for i in range(0, len(plate)):
        if (i in (0, 1, 5, 6) and not (plate[i] >= 'A' and plate[i] <= 'Z')):
            return False

        if (i in (2, 3, 4) and not (plate[i] >= '0' and plate[i] <= '9')):
            return False

    return True


#Check if Plate is in Car table.
def check_plate_existing(plate):
    """Check if plate exists"""
    #Open database connection.
    connection = sqlite3.connect('database/rent_a_car.db')

    #Prepare a cursor to work with database.
    cursor = connection.cursor()

    cursor.execute("SELECT * from car WHERE plate='" + plate + "'")
    cursor.fetchall()

    if cursor.rowcount == 0:
        return False

    return True


#Check if end date is correct
def check_end_date(DATE):
    """Check if a date is valid"""
    if not DATE:
        return False

    pattern = re.compile("([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))")
    if not bool(pattern.match(DATE)):
        return False

    if DATE < str(date.today()):
        return False

    return True


#Add new rent to database.
def add_rent(fiscal_code, plate, end_date):
    """Add rent to database"""
    #Open database connection.
    connection = sqlite3.connect('database/rent_a_car.db')

    #Prepare a cursor to work with database.
    cursor = connection.cursor()

    #Insert new client.
    try:
        start_date = date.today()
        cursor.execute("INSERT INTO rental(plate, fiscal_code, start_date, \
            end_date) \
            VALUES('"+ plate + "','" + fiscal_code + "','" + str(start_date) + "','" \
            + str(end_date) + "');")
        connection.commit()
        print("You have rent a car!")
    except:
        print("Sorry! An error occurred during the Rental!")
